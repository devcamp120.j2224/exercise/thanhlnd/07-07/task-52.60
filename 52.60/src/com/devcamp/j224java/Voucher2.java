package com.devcamp.j224java;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Voucher2 {
    public int id; // id của order
    public String customerName; // tên khách hàng
    public Integer price = 0;   // phải gán bằng số 
                 
    public Date date = new Date(); // ngày thực hiện order gán bằng 1 ngày new
    public Boolean confirm; // đã xác nhận hay chưa?
    public String[] items; // danh sách mặt hàng đã mua
    public Person buyer;// người mua, là một object thuộc class Person


    public Voucher2(){
        System.out.println("ko có tham số");
    }
    public Voucher2(int id , String customerName){
        this.id = id ;
        this.customerName = customerName ;
    }
    public Voucher2(int id , String customerName , Integer price , Date date){
        this.id = id ;
        this.customerName = customerName ;
        this.price = price;
        this.date = date ;
    }
    public Voucher2(int id , String customerName , Integer price , Date date , Boolean confirm , String[] items , Person buyer){
        this.id = id ;
        this.customerName = customerName;
        this.price = price ;
        this.date = date;
        this.items = items;
        this.buyer = buyer;
        this.confirm = true ; 
    }
    // làm method chơi 
    public void showData(){
        System.out.println("Id là : " +  this.id);
        System.out.println("customerName là : " +  this.customerName);
        System.out.println("Price là : " +  this.price);
        System.out.println("Date là : " +  this.date);
        System.out.println("Items là : " +  this.items);
        System.out.println("Buyer là : " +  this.buyer);
        System.out.println("Confirm là : " +  this.confirm);
    }

    @Override
    public String toString(){
        // định dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi" , "VN"));
        // định dạng cho ngày tháng
        // CÁCH 2 
        // LocalDateTime localDate = LocalDateTime.now(); // fixed: LocalDateTime
        // DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd hh:mm:ss");
         
        // CÁCH 1 
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss.SSS");

        // định dạng cho giá tiền 
        Locale locate = Locale.getDefault();
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locate);
        Long priceF = Long.valueOf(price) ;

        // return (trả ra ) chuỗi String

        return  "Order [id=" + id 
        + ", customer : " + customerName
        + ", price : " + numberFormat.format(priceF) 
        + ", date : " + formatter.format(date)
        + ", confirm: " + confirm
        + ", items : " + Arrays.toString(items)
        + ", person : " + buyer ;
    }
}
